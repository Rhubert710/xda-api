from flask import Flask, jsonify, request, render_template
import sqlite3
import json
import os
import psycopg2
from flask_caching import Cache

config = {
    "DEBUG": True,          # some Flask specific configs
    "CACHE_TYPE": "SimpleCache",  # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": 300
}

server = Flask(__name__)
# tell Flask to use the above defined config
server.config.from_mapping(config)
cache = Cache(server)

@server.route('/')
@cache.cached(timeout=50)
def hello_world():

    return render_template('index.html')


@server.get('/data')
def get_all_data():

    conn = psycopg2.connect(database="ddaln32mhhhaev", user='dysayonffssbzl', password='19ac40964fe9c7ff9131e48ce5aba9ee41d666e6d8bf79bb185d19927e84233b', host='ec2-44-198-196-149.compute-1.amazonaws.com', port= '5432')
    cur = conn.cursor()
    cur.execute('SELECT * FROM xda_postgres4;')
    data = cur.fetchall()
    cur.close()
    return jsonify(str(data))

@server.get('/data/<limit_arg>')
def get_data_limit(limit_arg):

    limit_formatted = format_limit(limit_arg)

    if 'ERROR' in limit_formatted:

        return render_template('error.html', error_message=limit_formatted)

    else:

        conn = psycopg2.connect(database="ddaln32mhhhaev", user='dysayonffssbzl', password='19ac40964fe9c7ff9131e48ce5aba9ee41d666e6d8bf79bb185d19927e84233b', host='ec2-44-198-196-149.compute-1.amazonaws.com', port= '5432')
        cur = conn.cursor()
        cur.execute(f'''SELECT * FROM xda_postgres4 {limit_formatted};''')
        data = cur.fetchall()
        cur.close()
        return str(data)


@server.get('/data/<limit_arg>/<colums_arg>')
def get_data_colums(limit_arg, colums_arg):

    limit_formatted = format_limit(limit_arg)
    colums_formatted = format_colums(colums_arg)
    errors = check_for_errors(limit_formatted, colums_formatted)

    if 'ERROR' in errors:
        return render_template('error.html', error_message=errors)

    else:

        conn = psycopg2.connect(database="ddaln32mhhhaev", user='dysayonffssbzl', password='19ac40964fe9c7ff9131e48ce5aba9ee41d666e6d8bf79bb185d19927e84233b', host='ec2-44-198-196-149.compute-1.amazonaws.com', port= '5432')
        cur = conn.cursor()
        cur.execute(f'''SELECT {colums_formatted} FROM xda_postgres4 {limit_formatted}''')
        data = cur.fetchall()
        cur.close()
        return str(data)


@server.get('/data/<limit_arg>/<colums_arg>/<from_date>')
def get_data_from_date(limit_arg, colums_arg, from_date):

    limit_formatted = format_limit(limit_arg)
    colums_formatted = format_colums(colums_arg)
    errors = check_for_errors(limit_formatted, colums_formatted)

    if 'ERROR' in errors:
        return render_template('error.html', error_message=errors)

    else:

        conn = psycopg2.connect(database="ddaln32mhhhaev", user='dysayonffssbzl', password='19ac40964fe9c7ff9131e48ce5aba9ee41d666e6d8bf79bb185d19927e84233b', host='ec2-44-198-196-149.compute-1.amazonaws.com', port= '5432')
        cur = conn.cursor()
        cur.execute(f'''SELECT {colums_formatted} FROM xda_postgres WHERE date_posted >= "{from_date}" {limit_formatted}''')
        data = cur.fetchall()
        cur.close()
        return str(data)


@server.get('/data/<limit_arg>/<colums_arg>/<from_date>/<to_date>')
def get_data_to_date(limit_arg, colums_arg, from_date, to_date):

    limit_formatted = format_limit(limit_arg)
    colums_formatted = format_colums(colums_arg)
    errors = check_for_errors(limit_formatted, colums_formatted)

    if 'ERROR' in errors:
        return render_template('error.html', error_message=errors)

    else:

        conn = psycopg2.connect(database="ddaln32mhhhaev", user='dysayonffssbzl', password='19ac40964fe9c7ff9131e48ce5aba9ee41d666e6d8bf79bb185d19927e84233b', host='ec2-44-198-196-149.compute-1.amazonaws.com', port= '5432')
        cur = conn.cursor()
        cur.execute(f'''SELECT {colums_formatted} FROM xda_postgres WHERE date_posted >= "{from_date}" and date_posted <= "{to_date}" {limit_formatted}''')
        data = cur.fetchall()
        cur.close()
        return str(data)



'''
functions
'''

def format_limit(lim_arg):

    limit_formatted = ""

    if lim_arg == '*':

        return ""

    if lim_arg.isdigit():

        return f'LIMIT {lim_arg}'

    else:
        return 'ERROR: {number_of_reults} must be a interger'


def format_colums(col_arg):

    col = []

    if col_arg == '*':
        return '*'
    else:

        if 'h' in col_arg.lower():
            col.append('headline_id')
        if 'e' in col_arg.lower():
            col.append('excerpt')
        if 'a' in col_arg.lower():
            col.append('author')
        if 'd' in col_arg.lower():
            col.append('date_posted')
        if ('h' not in col_arg.lower()) and ('e' not in col_arg.lower()) and ('a' not in col_arg.lower()) and ('d' not in col_arg.lower()):
            return 'ERROR: {data_to_return} must include h, e, a or d'

    col_str = col_str = str(col).replace("'", "")
    col_str = col_str.replace('[', "")
    col_str = col_str.replace(']', "")

    return col_str


def check_for_errors(limit_formatted, colums_formatted):
    if "ERROR" in limit_formatted:
        return limit_formatted
    if "ERROR" in colums_formatted:
        return colums_formatted
    return "ok"


if __name__ == '__main__':
    server.run(host='0.0.0.0', port=8000, debug=True)
