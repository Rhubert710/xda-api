import pytest
from app import *


def test_format_limit():
    assert format_limit('10') == 'LIMIT 10'

def test_format_limit_2():
    assert format_limit('*') == ''

def test_format_colums():
    assert format_colums('hd') == 'headline, date_posted'

def test_format_colums_2():
    assert format_colums('*') == '*'

def test_check_for_errors():
    assert check_for_errors('ERROR', 'ok') == 'ERROR'
